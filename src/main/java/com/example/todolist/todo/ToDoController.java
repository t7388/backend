package com.example.todolist.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/todo")
public class ToDoController {

    private final ToDoService toDoService;

    @Autowired
    public ToDoController(ToDoService toDoService) {
        this.toDoService = toDoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ToDo>> findAllToDos() {
        List<ToDo> toDos = toDoService.findAllToDos();
        return new ResponseEntity<>(toDos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ToDo> findToDoById(@PathVariable("id") Long id) {
        ToDo toDo = toDoService.findToDoById(id);
        return new ResponseEntity<>(toDo, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<ToDo> addToDo(@Valid @RequestBody ToDo toDo) {
        ToDo newToDo = toDoService.addToDo(toDo);
        return new ResponseEntity<>(newToDo, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<ToDo> updateToDo(@Valid @RequestBody ToDo toDo) {
        ToDo updateToDo = toDoService.updateToDo(toDo);
        return new ResponseEntity<>(updateToDo, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteToDo(@PathVariable("id") Long id) {
        toDoService.deleteToDo(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
