package com.example.todolist.todo;

import com.example.todolist.exception.ToDoNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ToDoServiceTest {

    @Mock
    ToDoRepository toDoRepository;
    ToDoService underTest;

    @BeforeEach
    void setUp() {
        underTest = new ToDoService(toDoRepository);
    }

    @Test
    void testFindAllToDos() {

        // when
        underTest.findAllToDos();

        // then
        verify(toDoRepository).findAll();

    }

    @Test
    void findToDoById() {

        // given
        ToDo toDo = createToDo();
        given(toDoRepository.findById(toDo.getId()))
                .willReturn(Optional.of(toDo));

        // when
        ToDo toDoById = underTest.findToDoById(toDo.getId());

        // then
        assertThat(toDoById).isEqualTo(toDo);

    }

    @Test
    void testThrowsToDoNotFoundException() {

        // given
        long id = Long.MAX_VALUE;

        // when

        // then
        assertThatThrownBy(() -> underTest.findToDoById(id))
                .isInstanceOf(ToDoNotFoundException.class)
                .hasMessageContaining("ToDo with id " + id + " was not found");

    }

    @Test
    void addToDo() {

        // given
        ToDo toDo = createToDo();

        // when
        underTest.addToDo(toDo);

        // then
        ArgumentCaptor<ToDo> toDoArgumentCaptor =
                ArgumentCaptor.forClass(ToDo.class);

        verify(toDoRepository).
                save(toDoArgumentCaptor.capture());

        ToDo capturedToDo = toDoArgumentCaptor.getValue();

        assertThat(capturedToDo).isEqualTo(toDo);

    }

    @Test
    void updateToDo() {

        // given
        ToDo toDo = createToDo();

        // when
        underTest.updateToDo(toDo);

        // then
        verify(toDoRepository).save(toDo);

    }

    @Test
    void deleteToDo() {

        // when
        underTest.deleteToDo(anyLong());

        // then
        verify(toDoRepository).deleteById(anyLong());

    }

    private ToDo createToDo() {

        ToDo toDo = new ToDo();
        toDo.setId(1L);
        toDo.setDescription("Example");
        toDo.setDueDate(LocalDate.now());
        toDo.setIsDone(false);

        return toDo;

    }

}
